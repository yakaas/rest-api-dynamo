package main

import (
	"fmt"
	"github.com/zenazn/goji"
	"github.com/zenazn/goji/web"
	"net/http"
)

func Bootstrap() {
	registerMiddlewares(goji.DefaultMux)
	registerHandlers(goji.DefaultMux)
}

func registerMiddlewares(mux *web.Mux) {
}

func registerHandlers(mux *web.Mux) {
	mux.Get("/health", health)
	mux.Get("/jobs/:id", getJob)
	mux.Get("/jobs/filter/:ids", getJobs)
}

func health(c web.C, w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "OK")
}
