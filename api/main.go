package main

import (
//	"flag"
	"github.com/zenazn/goji"
	"github.com/zenazn/goji/web/middleware"
)

func main() {
	LoadConfig()
	goji.Abandon(middleware.AutomaticOptions)

	Bootstrap()

//	flag.Set("bind", ":1234")
	goji.Serve()
}
