package repository

type JobsBatch struct {
	Jobs []*Job
}

type Advertiser struct {
	Id          int    `json:"id"`
	Description string `json:"name"`
}

type Area struct {
	Id          int    `json:"id"`
	Description string `json:"name"`
}

type WrapperLocation struct {
	Location Location `json:"location"`
}

type Location struct {
	Id          int    `json:"id"`
	Description string `json:"name"`
	Display     string `json:"display"`
	Area        Area
}

type Job struct {
	Id         string `json:"jobId"`
	Title      string `json:"title"`
	Listed     string `json:"createDate"`
	Status     string `json:"status"`
	Advertiser Advertiser
	Location   WrapperLocation
	SalaryText string `json:"salary.additionalText"`
}
