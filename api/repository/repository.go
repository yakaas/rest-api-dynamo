package repository

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type Repository interface {
	GetJobs([]string) (*JobsBatch, error)
	GetJob(string) (*Job, error)
}

type AWSClient struct {
	config      *aws.Config
	dynamoTable string
	dynamodb    dynamoService
}

func NewRepository(config *aws.Config, dynamoTable string) Repository {
	return &AWSClient{config: config, dynamodb: newDynamoAWSService(config), dynamoTable: dynamoTable}
}

func (client *AWSClient) GetJob(id string) (*Job, error) {

	params := &dynamodb.GetItemInput{
		Key:                      map[string]*dynamodb.AttributeValue{"jobId": {S: aws.String(id)}},
		ProjectionExpression:     aws.String("jobId, title,createDate, advertiser.id, advertiser.#name, #status, #location.#location.id, #location.#location.#name, #location.#location.display, #location.#area.id, #location.#area.#name, salary.additionalText"),
		ExpressionAttributeNames: map[string]*string{"#status": aws.String("status"), "#name": aws.String("name"), "#location": aws.String("location"), "#area": aws.String("area")},
		TableName:                aws.String(client.dynamoTable),
	}

	resp, err := client.dynamodb.GetItem(params)
	if err != nil {
		return nil, err
	}

	job := &Job{}
	if err := dynamodbattribute.UnmarshalMap(resp.Item, job); err != nil {
		return nil, err
	}

	return job, nil
}

func (client *AWSClient) GetJobs(ids []string) (*JobsBatch, error) {

	keys := make([]map[string]*dynamodb.AttributeValue, len(ids))
	for i := 0; i < len(ids); i++ {
		keys[i] = make(map[string]*dynamodb.AttributeValue)
		keys[i]["jobId"] = &dynamodb.AttributeValue{S: aws.String(ids[i])}
	}

	params := &dynamodb.BatchGetItemInput{
		RequestItems: map[string]*dynamodb.KeysAndAttributes{
			client.dynamoTable: {
				Keys: keys, ConsistentRead: aws.Bool(true),
				ProjectionExpression:     aws.String("jobId, title,createDate, advertiser.id, advertiser.#name, #status, #location.#location.id, #location.#location.#name, #location.#location.display, #location.#area.id, #location.#area.#name, salary.additionalText"),
				ExpressionAttributeNames: map[string]*string{"#status": aws.String("status"), "#name": aws.String("name"), "#location": aws.String("location"), "#area": aws.String("area")},
			},
		},
		ReturnConsumedCapacity: aws.String("NONE"),
	}

	resp, err := client.dynamodb.BatchGetItem(params)
	if err != nil {
		return nil, err
	}

	jobs := make([]*Job, len(resp.Responses[client.dynamoTable]))
	for i := range resp.Responses[client.dynamoTable] {
		job := &Job{}
		if err := dynamodbattribute.UnmarshalMap(resp.Responses[client.dynamoTable][i], job); err != nil {
			return nil, err
		}

		jobs[i] = job
	}

	return &JobsBatch{Jobs: jobs}, nil
}
