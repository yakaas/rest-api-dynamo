package repository

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

type dynamoService interface {
	GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error)
	BatchGetItem(input *dynamodb.BatchGetItemInput) (*dynamodb.BatchGetItemOutput, error)
}

type dynamoAWSService struct {
	svc *dynamodb.DynamoDB
}

func newDynamoAWSService(c *aws.Config) dynamoService {
	svc := dynamodb.New(session.New(c))
	return &dynamoAWSService{svc}
}

func (d *dynamoAWSService) GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {
	return d.svc.GetItem(input)
}

func (d *dynamoAWSService) BatchGetItem(input *dynamodb.BatchGetItemInput) (*dynamodb.BatchGetItemOutput, error) {
	return d.svc.BatchGetItem(input)
}
