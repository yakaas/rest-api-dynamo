package main

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/defaults"
	"github.com/jacobstr/confer"
	"net/http"
	"bitbucket.org/yakaas/rest-api-dynamo/api/repository"
	"time"
)

var (
	config        = &Config{Config: confer.NewConfig()}
	dynamoTimeout = time.Duration(2 * time.Second)
	dynamoTable   = "mobile-jobdetails-projection-api-datastore"
	awsRegion     = "ap-southeast-2"
)

type Config struct {
	*confer.Config
	repository repository.Repository
}

func LoadConfig() {
	c := defaults.Config().WithRegion(awsRegion)
	c.HTTPClient = &http.Client{Timeout: dynamoTimeout}
	c.Credentials = defaults.CredChain(c, defaults.Handlers())

	config.Set("app.aws.config", c)
}

func (c *Config) Repository() repository.Repository {
	if c.repository == nil {
		c.repository = repository.NewRepository(c.GetAWSConfig(), dynamoTable)
	}
	return c.repository
}

func (c *Config) GetAWSConfig() *aws.Config {
	return config.Get("app.aws.config").(*aws.Config)
}
