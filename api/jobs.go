package main

import (
	"encoding/json"
	"fmt"
	"github.com/zenazn/goji/web"
	"net/http"
	"strings"
)

func getJob(c web.C, w http.ResponseWriter, r *http.Request) {

	id, _ := c.URLParams["id"]

	job, err := config.Repository().GetJob(id)
	if err != nil {
		fmt.Fprint(w, "ERROR", err)
		return
	}

	enc := json.NewEncoder(w)
	if err := enc.Encode(job); err != nil {
		fmt.Fprint(w, "ERROR", err)
		return
	}
}

func getJobs(c web.C, w http.ResponseWriter, r *http.Request) {

	params, _ := c.URLParams["ids"]
	ids := strings.Split(params, ",")

	jobs, err := config.Repository().GetJobs(ids)
	if err != nil {
		fmt.Fprint(w, "ERROR", err)
		return
	}

	enc := json.NewEncoder(w)
	if err := enc.Encode(jobs); err != nil {
		fmt.Fprint(w, "ERROR", err)
		return
	}
}
