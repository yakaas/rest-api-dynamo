#!/bin/bash -e

function failToAuth {
    echo "Unable to reach dynamodb!"
    echo "ensure you've authorised with AWS"
    exit 1
}

aws dynamodb list-tables &> /dev/null || failToAuth

./api/api
