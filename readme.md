# Go, DynamoDB, API and some more

## Endpoints:

- Get job by id: <host>:8000/jobs/{id}
- Get jobs by id sequence: <host>:8000/jobs/filter/{id,id,....}
- Health - <host>:8000/health

## Requirements

- Git
- Curl
- GO
- AWS

## Setup

- git clone git@bitbucket.org:yakaas/rest-api-dynamo.git
- cd rest-api-demo
- ./build.sh && ./run.sh


